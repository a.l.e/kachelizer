# Kachelizer

Split a picture in tiles of a specific size.

## Development

See the kanban for the next / planned tasks.

<https://doc.qt.io/qt-5/qml-tutorial2.html>

Using in C++ the qml files from the second step in the Qt.io qml tutorial.

- two columns (photo and settins): [RowLayout QML Type](https://doc.qt.io/qt-5/qml-qtquick-layouts-rowlayout.html)
- putting a grid on top of the image: [Is it possible to create a map grid in qml?](https://stackoverflow.com/questions/46641161/is-it-possible-to-create-a-map-grid-in-qml)
- set the image size depending on the print size and the number of columns and rows:
  - [How can I use QML Layouts to arrange aspect ratio scaled items in a grid?](https://stackoverflow.com/questions/41431941/how-can-i-use-qml-layouts-to-arrange-aspect-ratio-scaled-items-in-a-grid)
- Using a toast-alike popup: <https://stackoverflow.com/a/43797808/5239250>
- using internal components: https://doc.qt.io/qt-5/qml-qtqml-component.html
- [Settings](https://doc.qt.io/qt-5/qml-qt-labs-settings-settings.html)
- [On using named icons](https://doc.qt.io/qt-5/qtquickcontrols2-icons.html)

For my latest production i've used <https://posterazor.sourceforge.io/online/> but i aim at doing better...

## Image slicing


- [How to save a QPixmap Object to a file?](https://stackoverflow.com/questions/2430877/how-to-save-a-qpixmap-object-to-a-file)
- `QPixmap QPixmap::copy(int x, int y, int width, int height) const`
- C++ action on QML signal: [Connecting to QML Signals](https://doc.qt.io/qt-5/qtqml-cppintegration-interactqmlfromcpp.html#connecting-to-qml-signals)
