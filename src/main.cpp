#include <optional>

#include <QObject>
#include <QGuiApplication>
#include <QIcon>
#include <QQmlApplicationEngine>
#include <QCommandLineParser>
#include <QFileInfo>
#include <QQmlContext>

#include "backend.h"

int main(int argc, char** argv)
{

    std::optional<QString> input_file{};
    // std::optional<QString> output_dir{};

    QGuiApplication app(argc, argv);
    QGuiApplication::setApplicationName("kachelizer");
    QGuiApplication::setApplicationVersion("0.1");
    // app.setOrganizationName("kachelizer");
    QGuiApplication::setOrganizationDomain("org.graphicslab");

    QIcon::setThemeName("kachelizer");


    QCommandLineParser parser;
    parser.setApplicationDescription("Create printable tiles out of a picture");


    parser.addPositionalArgument("input", QCoreApplication::translate("main", "Input picture file."));
    // parser.addOptions({
    //     {{"o", "output"},
    //         QCoreApplication::translate("main", "Output <directory>."),
    //         QCoreApplication::translate("main", "directory")},
    // });
    parser.addHelpOption();
    parser.addVersionOption();

    parser.process(app);

    const QStringList args = parser.positionalArguments();
    if (args.size() > 0) {
        input_file = args.at(0);
    }

    // output_dir = parser.value("output");

    Backend backend{};

    QQmlApplicationEngine engine;

    engine.rootContext()->setContextProperty("backend", &backend);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    if (input_file) {
        if (!backend.set_image(*input_file)) {
            qCritical() << "The input file does not exist: " << qPrintable(*input_file);
            return 0;
        }
    }

    return app.exec();
}
