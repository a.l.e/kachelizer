import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

GroupBox {
    property int printWidth: parseInt(printWidth.text)
    property int printHeight: parseInt(printHeight.text)
    property alias currentColor: colorPicker.currentColor
    Settings {
        property alias printWidth: printWidth.text
        property alias printHeight: printHeight.text
    }
    Layout.fillWidth: true
    Layout.minimumWidth: 50
    Layout.preferredWidth: 100
    Layout.maximumWidth: 300
    Layout.fillHeight: true
    Layout.minimumHeight: 150
    title: "Configure"
    ColumnLayout {
        Layout.fillWidth: true
        Layout.fillHeight: true
        RowLayout {
            width:  parent.width
            // Layout.fillWidth: true
            Button {
                text: qsTr("Load image")
                onClicked: {
                    fileDialog.open();
                }
            }
            Item {
                Layout.fillWidth: true
            }
            ToolButton {
                id:buttonAbout
                icon.name: "settings"
                Layout.alignment: Qt.AlignRight
                onClicked: showSettings()
            }
        }
        Label {
            text: qsTr("Print width")
        }
        RowLayout {
            TextField {
                id: printWidth
                text: "9"
            }
            Label {
                text: qsTr("cm")
            }
        }
        Label {
            text: qsTr("Print height")
        }
        RowLayout {
            TextField {
                id: printHeight
                text: "13"
            }
            Label {
                text: qsTr("cm")
            }
        }
        RowLayout {
        }
        Label {
            text: qsTr("Rows")
        }
        SpinBox {
            id: rows
            from: 1
            editable: true
            onValueChanged: {
                rowsCount = this.value
            }
        }
        Label {
            text: qsTr("Columns")
        }
        SpinBox {
            id: columns
            from: 1
            editable: true
            onValueChanged: {
                columnsCount = this.value
            }
        }
        Label {
            text: qsTr("Grid color")
        }
        ColorPicker {
            id: colorPicker
            onCurrentColorChanged: grid.requestPaint()
        }
        Label {
            text: qsTr("Zoom")
        }
        RowLayout {
            TextField {
                id: zoom
            }
            Label {
                text: qsTr("%")
            }
        }
        Button {
            id: produce
            signal qmlSignal(msg: string)
            text: qsTr("Produce")
            onClicked: backend.slice(photo.source,
                parseInt(rows.value),
                parseInt(columns.value),
                photoPane.width / ratio,
                photoPane.height / ratio,
                photoPane.contentX / ratio,
                photoPane.contentY / ratio)
        }
    }
}
