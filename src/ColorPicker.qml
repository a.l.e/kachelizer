import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3

RowLayout {
    property string currentColor: "white"
    property int currentIndex: 1

    Repeater {
        model: ["black", "white", "red"]
        Button {
            background: Rectangle {
                color: modelData
                border.color: index == currentIndex ? "lightgray" : "darkgray"
                border.width: index == currentIndex ? 4 : 1
                implicitWidth: index == currentIndex ? 40 : 37
                implicitHeight: index == currentIndex ? 40 : 37
                radius: 2
            }
            onClicked: {
                currentIndex = index
                currentColor = modelData
            }
        }
    }
}
