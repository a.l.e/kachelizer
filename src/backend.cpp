#include "backend.h"

#include <QDebug>
#include <QPixmap>
#include <QSettings>
#include <QString>
#include <QFile>
#include <QUrl>
#include <QFileInfo>
#include <QDir>
#include <QDateTime>

Backend::Backend(QObject *parent) : QObject(parent)
{
}

bool Backend::set_image(const QString filename)
{
    auto fileinfo = QFileInfo(filename);
    if (!fileinfo.isFile()) {
        return false;
    }
    emit setPhoto("file:" + fileinfo.absoluteFilePath());
    return true;
}

void Backend::slice(const QString& path, int rows, int columns, int width, int height, float x_offset, float y_offset)
{
    // qDebug() << path << rows << columns << x_offset << y_offset;

    // get the output path:
    // if target is 1, then it's next to the original picture
    // if target is 2, then use the outputPath from the settings.
    QSettings settings;
    int output_target = settings.value("Settings/outputTarget", 0).toInt();
    QString output_path = settings.value("Settings/outputPath", "").toString();
    qDebug() << output_target << output_path;

    QFileInfo fileinfo(QUrl(path).toLocalFile());
    if (!fileinfo.exists() || !fileinfo.isFile()) {
        emit showMessage("File does not exist: " + QUrl(path).toLocalFile());
        return;
    }

    QDir parent_dir;

    if (output_target == 0) {
        parent_dir = fileinfo.dir();
    } else if (output_target == 1) {
        QFileInfo output_target_fileinfo;
        output_target_fileinfo.setFile(output_path);
        if (!output_target_fileinfo.isDir()) {
            emit showMessage("The output path does not exist: " + QUrl(output_path).toLocalFile());
            return;
        }
        parent_dir = QDir(output_path);
    } else {
        emit showMessage("Invalid target mode.");
        return;
    }
    qDebug() << "parent_dir" << parent_dir;

    QString output_directory = "output-"+QDateTime::currentDateTime().toString("yyyy-MM-dd-hh-mm-ss");
    if (!parent_dir.mkdir(output_directory)) {
        emit showMessage("Could not create: " + output_directory);
        return;
    }
    if (!parent_dir.cd(output_directory)) {
        emit showMessage("Could not use: " + output_directory);
        return;
    }

    QPixmap original{};
    if (!original.load(QUrl(path).toLocalFile())) {
        emit showMessage("Could not get the file: " + QUrl(path).toLocalFile());
        return;
    }

    int crop_width = width / columns;
    int crop_height = height / rows;
    int digits = qMax(columns, rows) < 100 ? 2 : 4; // sane approximation
    QChar fill = QChar('0');
    QString output_pattern = fileinfo.baseName() + "-%1-%2.jpg";
    for (int j = 0; j < rows; j++) {
        for (int i = 0; i < columns; i++) {
            QRect crop(x_offset + crop_width * i, y_offset + crop_height * j, crop_width, crop_height);
            QPixmap cropped = original.copy(crop);
            QString output_filename = output_pattern
                .arg(j + 1, digits, 10, fill)
                .arg(i + 1, digits, 10, fill);
            qDebug() << output_filename;
            QFile file(parent_dir.filePath(output_filename));
            file.open(QIODevice::WriteOnly);
            cropped.save(&file, "JPEG");
        }
    }
    emit showMessage("Images created in: " + output_directory);
}
