import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0

ApplicationWindow
{
    id: mainWindow
    objectName: "mainWindow"
    visible: true
    property int margin:  11
    width: mainPane.implicitWidth + 2 * margin
    height: mainPane.implicitHeight + 2 * margin
    minimumWidth: mainPane.Layout.minimumWidth + 2 * margin
    minimumHeight: mainPane.Layout.minimumHeight + 2 * margin
    title: qsTr("Kachelizer")

    // Settings {
    //     property alias printWidth: configure.printWidth.text
    //     property alias printHeight: configure.printHeight.text
    // }

    Shortcut {
        sequences: [StandardKey.Quit, StandardKey.Cancel, StandardKey.Close]
        context: Qt.ApplicationShortcut
        onActivated: Qt.quit()
    }

    property var columnsCount: 1
    onColumnsCountChanged: update()
    property var rowsCount: 1
    onRowsCountChanged: update()
    property var ratio: 0

    Connections {
        target: backend
        function onShowMessage(message) {
            toast.show(message);
        }
        function onSetPhoto(path) {
            setPhoto(path);
        }
    }

    function setPhoto(path) {
        photo.source = path
        update()
    }

    function update() {
        const innerWidth = columnsCount * parseInt(configure.printWidth)
        const innerHeight = rowsCount * parseInt(configure.printHeight)
        const innerRatio = innerWidth / innerHeight
        const outerRatio = mainPane.width / mainPane.height

        const scale = innerRatio >= outerRatio ?
            mainPane.width / innerWidth :
            mainPane.height / innerHeight

        photoPane.width = innerWidth * scale
        photoPane.height = innerHeight * scale

        const ratioW = photoPane.width / photo.sourceSize.width
        const ratioH = photoPane.height / photo.sourceSize.height

        ratio = ratioW > ratioH ? ratioW : ratioH

        photoPane.contentWidth = photo.sourceSize.width * ratio
        photoPane.contentHeight = photo.sourceSize.height * ratio
        photo.width = photo.sourceSize.width * ratio
        photo.height = photo.sourceSize.height * ratio

        grid.requestPaint()
    }

    function showSettings() {
        configure.visible = false
        settings.visible = true
    }

    function showConfiguration() {
        settings.visible = false
        configure.visible = true
    }

    ToastManager {
        id: toast
    }

	FileDialog{
        id: fileDialog;
        title: "Please choose a file";
        nameFilters: ["Image Files (*.jpg *.png *.gif)"];
        selectExisting: true
        visible: false
        onAccepted: {
            setPhoto(fileDialog.fileUrl)
            fileDialog.close()
        }
        Component.onCompleted: visible = false
    }

    RowLayout {
        anchors.fill: parent
        Rectangle {
            id: mainPane
            // width: 320; height: 480
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumWidth: 50
            Layout.preferredWidth: 100
            // Layout.maximumWidth: 300
            Layout.minimumHeight: 150
            anchors.margins: mainWindow.margin
            color: "darkgray"

            Flickable {
                id: photoPane
                anchors.centerIn: parent
                visible: photo.source !== ""
                clip: true
                Image {
                    id: photo
                }
            }
            Canvas {
                id: grid
                anchors.fill : photoPane
                onPaint: {
                    var ctx = getContext("2d")
                    ctx.reset();
                    ctx.lineWidth = 1
                    ctx.strokeStyle = configure.currentColor
                    ctx.beginPath()

                    const column = width / columnsCount
                    const x = photo.x
                    const y = photo.y

                    // TODO: see also https://stackoverflow.com/questions/13384594/fit-a-rectangle-into-another-rectangle for fitting
                    for (var i = 1; i < columnsCount; i++) {
                        ctx.moveTo(x + i * column, y);
                        ctx.lineTo(x + i * column, y + height);
                    }
                    const row = height / rowsCount
                    for (var i = 1; i < rowsCount; i++) {
                        ctx.moveTo(x, y + i * row);
                        ctx.lineTo(x + width, y+ i * row);
                    }

                    ctx.closePath()
                    ctx.stroke()
                }
            }
            Text {
                id: debugText
                visible: false
                text: 'source: %1 x %2\n'.arg(photo.sourceSize.width).arg(photo.sourceSize.height) +
                      'pane: %1 x %2\n'.arg(photoPane.width).arg(photoPane.height) +
                      'photo: %1 x %2\n'.arg(photo.width).arg(photo.height) +
                      'dx: %1, dy %2'.arg(photoPane.contentX).arg(photoPane.contentY)
                color: 'darkred'
            }
        }
        Configure {
            id: configure
            visible: true
        }
        Settings {
            id: settings
            visible: false
        }
    }
}
