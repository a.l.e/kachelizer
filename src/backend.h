#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>

class Backend : public QObject
{
    Q_OBJECT
    public:
        explicit Backend(QObject *parent = nullptr);
        bool set_image(const QString filename);

    signals:
        void showMessage(const QString& warning);
        void setPhoto(const QString& path);
    public slots:
        void slice(const QString& path, int rows, int columns, int width, int height, float x_offset, float y_offset);
};

#endif
