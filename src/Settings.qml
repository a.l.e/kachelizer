import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

GroupBox {
    Settings {
        id: settings
        category: 'Settings'
        property int unit
        property int outputTarget
        property alias outputPath: outputPath.text
    }
    Layout.fillWidth: true
    Layout.minimumWidth: 50
    Layout.preferredWidth: 100
    Layout.maximumWidth: 300
    Layout.fillHeight: true
    Layout.minimumHeight: 150
    title: "Settings"
    ColumnLayout {
        Layout.fillWidth: true
        Layout.fillHeight: true
        RowLayout {
            width:  parent.width
            // Layout.fillWidth: true
            Item {
                Layout.fillWidth: true
            }
            ToolButton {
                id:buttonAbout
                icon.name: "back"
                Layout.alignment: Qt.AlignRight
                onClicked: showConfiguration()
            }
        }
        Label {
            text: qsTr("Target directory")
        }
        Repeater {
            model: ['Next to the picture', 'Fixed path']
            RadioButton {
                text: modelData
                Binding on checked { value: settings.outputTarget == index }
                onClicked: {
                    settings.outputTarget = index
                }
            }
        }
        TextField {
            id: outputPath
            visible: settings.outputTarget == 1
            text: ''
        }
        Label {
            text: qsTr("Unit")
        }
        // https://stackoverflow.com/questions/38798450/qt-5-7-qml-why-are-my-checkbox-property-bindings-disappearing
        Repeater {
            model: ['cm', 'mm', 'in']
            RadioButton {
                text: modelData
                Binding on checked { value: settings.unit == index }
                onClicked: {
                    settings.unit = index
                }
            }
        }
    }
}
